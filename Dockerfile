FROM node:20 as build
WORKDIR /home/node/movies-web
COPY package.json package-lock.json ./
RUN npm install
COPY . .
ARG REACT_APP_API_URL
RUN npm run build

FROM nginx:1.23-alpine
COPY --from=build /home/node/movies-web/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf