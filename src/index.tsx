import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import App from "./App";
import "./index.css";
import FavouritePage from "./Pages/FavouritePage/FavouritePage";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import MovieDetailsPage from "./Pages/MovieDetailsPage/MovieDetailsPage";
import Page404 from "./Pages/Page404/Page404";
import ProfilePage from "./Pages/ProfilePage/ProfilePage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import SearchResultsPage from "./Pages/SearchResultsPage/SearchResultsPage";
import store from "./store";

const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);

const router = createBrowserRouter([
    {
        path: "/",
        element: <App />,
        children: [
            {
                path: "",
                element: <HomePage />,
            },
            {
                path: "my-profile",
                element: <ProfilePage />,
            },
            {
                path: "favourites",
                element: <FavouritePage />,
            },
            {
                path: "search",
                element: <SearchResultsPage />,
            },
            {
                path: "movies/:movieId",
                element: <MovieDetailsPage />
            },
            {
                path: "register",
                element: <RegisterPage />
            },
            {
                path: "login",
                element: <LoginPage />
            },
            {
                path: "*",
                element: <Page404 />
            },
        ]
    },
]);

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router} />
        </Provider>
    </React.StrictMode>
);
