
export function bodyOrThrow<T>(): (r: Response) => Promise<T> {

    return resp => {
        if(!resp.ok) {
            return resp
                .json()
                .then(err => Promise.reject<T>(err));
        }

        return resp.json();
    };
}


export function okOrThrow(resp: Response): Promise<void> {
    if(!resp.ok) {
        return resp
            .json()
            .then(err => { throw err; });
    }

    return Promise.resolve();
}
