import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import './Page404.css';

function Page404() {
    return (
        <Container className="main page-404">
            <Row>
                <Col>
                    <div className="d-flex justify-content-center flex-column align-items-center">
                        <h1 className="err">404</h1>
                        <div className="msg">Maybe this page moved? Got deleted? Anyway how did you get here?
                            <p>Let's go <Link to="/">Home</Link> and try from there.</p>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

export default Page404