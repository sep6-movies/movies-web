import { useEffect } from "react";
import { Alert, Col, Container, Image, Placeholder, PlaceholderProps, Row, Spinner } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { JSX } from "react/jsx-runtime";
import CollapsableGrid from "../../Components/CollapsableGrid/CollapsableGrid";
import FavouriteButton from "../../Components/FavouritesButton/FavouritesButton";
import Header from "../../Components/Header/Header";
import RatingSummaryView from "../../Components/RatingSummaryView/RatingSummaryView";
import ReviewView from "../../Components/ReviewView/ReviewView";
import { useAppSelector } from "../../hooks";
import { useMovieDetails } from "../../hooks/movieHooks";
import { useMovieReviews, useUserReview } from "../../hooks/reviewHooks";
import "./MovieDetailsPage.css";

const TextPlaceholder = (props: JSX.IntrinsicAttributes & PlaceholderProps) => {
    const innerProps = {
        ...props,
        as: undefined,
    };

    return (
        <Placeholder as={props.as} animation="glow">
            <Placeholder {...innerProps} className="w-100" />
        </Placeholder>
    );
}

export default function MovieDetailsPage() {
    const { movieId } = useParams();
    const nav = useNavigate();
    const user = useAppSelector(s => s.auth.details);
    const [movie, updateMovie, loading, error] = useMovieDetails(movieId);
    const [review, updateReview, updating] = useUserReview(movieId);
    const [reviews] = useMovieReviews(movieId);

    useEffect(() => {
        if(!movieId)
            nav('/');
    }, [movieId, nav]);

    if(error) {
        return (
            <Container className="main">
                <Row>
                    <Col>
                        <Alert variant="danger">Failed to fetch movie details: {error}</Alert>
                    </Col>
                </Row>
            </Container>
        );
    }

    return (
        <Container className="main d-flex flex-column gap-4 mb-4">
            <Header header={loading
                        ? <TextPlaceholder as="h1" />
                        : <h1>{movie?.title}</h1>}>

                {user && <FavouriteButton id={movie?.id} />}
            </Header>

            <Row>
                <Col xs={12} md={4} className="d-flex justify-content-center align-items-center">
                    {loading
                        ? <Spinner animation="border" style={{ margin: "100px 0" }} />
                        : <Image src={movie?.posterUrl} className="mb-3" fluid rounded />}
                </Col>

                <Col>
                    {loading
                        ? Array.from({ length: 5 }).map((_, i) => <TextPlaceholder key={i} />)
                        : <p className="fs-5">{movie?.description}</p>}
                </Col>
            </Row>

            <Row>
                <Col>
                    <RatingSummaryView summary={movie?.rating} />
                </Col>
            </Row>

            <Header header={<h2>Reviews:</h2>} />

            {user && <Row>
                <Col>
                    <ReviewView
                        review={review}
                        loading={updating}
                        onRatingChange={rating => updateMovie({ ...movie!, rating })}
                        onReviewChange={updateReview}
                        className="shadow" />
                </Col>
            </Row>}

            <Row>
                <Col>
                    {reviews?.length === 0 && <span>No reviews yet</span>}

                    {reviews?.filter(r => r.reviewerId !== user?.id).map((r, idx) => <ReviewView
                        key={idx}
                        review={r}
                        className="mb-3"
                        readOnly />)}
                </Col>
            </Row>

            <Row>
                <Col>
                    <CollapsableGrid
                        header="Cast"
                        items={movie?.cast.map(m => ({
                            title: m.name,
                            description: m.character,
                            image: m.profile_path
                        }))}
                        loading={loading} />
                </Col>
            </Row>

            <Row>
                <Col>
                    <CollapsableGrid
                        header="Crew"
                        items={movie?.crew.map(m => ({
                            title: m.name,
                            description: m.job,
                            image: m.profile_path
                        }))}
                        loading={loading} />
                </Col>
            </Row>
        </Container>
    );
}
