import { Col, Container, Row } from "react-bootstrap";
import CenteredSpinner from "../../Components/CenteredSpinner/CenteredSpinner";
import UserDetailsForm from "../../Components/UserDetailsForm/UserDetailsForm";
import { useUserDetails } from "../../hooks/userHooks";
import './ProfilePage.css';

function ProfilePage() {

    const [details, update] = useUserDetails();

    return (
        <Container className="main">
            <Row className="justify-content-center">
                <Col md={9} lg={6}>
                    {details
                        ? (
                            <UserDetailsForm
                                title="Update your profile"
                                buttonLabel="Update"
                                details={details}
                                onSubmit={update} />
                        ) : (
                            <CenteredSpinner />
                        ) }
                </Col>
            </Row>
        </Container>
    );
}

export default ProfilePage;