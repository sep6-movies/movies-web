import { Alert, Col, Container, Row } from "react-bootstrap";
import CenteredSpinner from "../../Components/CenteredSpinner/CenteredSpinner";
import Header from "../../Components/Header/Header";
import MovieList from "../../Components/MovieList/MovieList";
import { useFavourites } from "../../hooks/favouriteHooks";

function FavouritePage() {
    const [favourites, error, loading] = useFavourites();

    return (
        <Container className="main">
            <Header header="Favourite movies" />

            {error && <Alert variant="danger" dismissible>
                {error}
            </Alert>}

            <Row>
                <Col>
                    {loading
                        ? <CenteredSpinner />
                        : <MovieList movies={favourites ?? []} />}
                </Col>
            </Row>
        </Container>
    );

}

export default FavouritePage;