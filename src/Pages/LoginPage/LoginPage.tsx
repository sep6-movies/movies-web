import { Col, Container, Row } from "react-bootstrap";
import LoginForm from "../../Components/LoginForm/LoginForm";

export default function LoginPage() {

    return (
        <Container className="main">
            <Row className="justify-content-center">
                <Col md={9} lg={6}>
                    <LoginForm />
                </Col>
            </Row>
        </Container>
    );
}
