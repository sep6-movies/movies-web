import { Col, Container, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import UserDetailsForm from "../../Components/UserDetailsForm/UserDetailsForm";
import { UpdateUserDto, useRegisterUser } from "../../hooks/userHooks";

export default function RegisterPage() {

    const register = useRegisterUser();
    const nav = useNavigate();

    const handleRegister = (dto: UpdateUserDto) => register(dto)
        .then(() => nav('/login'));

    return (
        <Container className="main">
            <Row className="justify-content-center">
                <Col md={9} lg={6}>

                    <UserDetailsForm
                        title="Register"
                        onSubmit={handleRegister} />

                </Col>
            </Row>
        </Container>
    );
}
