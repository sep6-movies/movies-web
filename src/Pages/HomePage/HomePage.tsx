import { Alert, Col, Container, Row } from "react-bootstrap";
import Header from "../../Components/Header/Header";
import MovieCarousel from "../../Components/MovieCarousel/MovieCarousel";
import { usePopularMovies } from "../../hooks/movieHooks";
import "./HomePage.css";

function HomePage() {

    const [popular, error] = usePopularMovies();

    return (
        <Container className="main">
            <Header header="Popular" />
            
            <Row>
                <Col>
                    {error
                        ? <Alert variant="danger">{error}</Alert>
                        : <MovieCarousel movies={popular ?? []} />}
                </Col>
            </Row>
        </Container>
    );
}

export default HomePage;
