import { useEffect } from "react";
import { Alert, Col, Container, Row } from "react-bootstrap";
import { useNavigate, useSearchParams } from "react-router-dom";
import CenteredSpinner from "../../Components/CenteredSpinner/CenteredSpinner";
import Header from "../../Components/Header/Header";
import MovieList from "../../Components/MovieList/MovieList";
import { useMovieSearch } from "../../hooks/movieHooks";
import './SearchResultsPage.css';

function SearchResultsPage() {

    const [params] = useSearchParams();
    const nav = useNavigate();
    const [results, search, error, loading] = useMovieSearch();

    useEffect(() => {
        const query = params.get('query');
        if (!query) {
            nav('/');
            return;
        }

        search(query);
    }, [params, nav, search]);

    if (error) {
        return (
            <Col xs={12}><Alert variant="danger">{error}</Alert></Col>
        );
    }

    return (
        <Container className="main search-results">
            <Header header="Search" />

            <Row>
                <Col>
                    {loading
                        ? <CenteredSpinner />
                        : <MovieList movies={results ?? []} />}
                </Col>
            </Row>
        </Container>
    );
}

export default SearchResultsPage;
