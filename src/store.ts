import { configureStore } from '@reduxjs/toolkit';
import authReducer from './slices/authSlice';
import favReducer from './slices/favSlice';

const store = configureStore({
    reducer: {
        auth: authReducer,
        fav: favReducer,
    }
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;