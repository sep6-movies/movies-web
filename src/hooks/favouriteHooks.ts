import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { config } from "../config";
import { useAppDispatch, useAppSelector } from "../hooks";
import { AuthState } from "../slices/authSlice";
import { favourite, setFavourites, unFavourite } from "../slices/favSlice";
import { bodyOrThrow, okOrThrow } from "../utils";
import { MovieSummary } from "./movieHooks";
import { useAuthenticatedFetch } from "./userHooks";

export function useFavourites(): [Array<MovieSummary> | undefined, string | undefined, boolean] {
    const authFetch = useAuthenticatedFetch();
    const user = useAppSelector(s => s.auth.details);
    const nav = useNavigate();
    const [favourites, setFavourites] = useState<Array<MovieSummary> | undefined>();
    const [err, setErr] = useState<string | undefined>();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setLoading(true);
        authFetch(`${config.API_URL}/users/${user?.id}/favourites`)
            .then(bodyOrThrow<Array<MovieSummary>>())
            .then(movies => setFavourites(movies))
            .catch(err => setErr(err.message))
            .finally(() => setLoading(false));
    }, [user, nav, authFetch]);

    return [favourites, err, loading];
}

export function useAddToFavourites(): (id: string) => Promise<void> {
    const authFetch = useAuthenticatedFetch();
    const dispatch = useAppDispatch();
    const userDetails = useAppSelector(s => s.auth.details);

    return movieId => authFetch(`${config.API_URL}/users/${userDetails?.id}/favourites`, {
        method: 'POST',
        headers: { "Content-Type": "application/json", },
        body: JSON.stringify({ movieId }),
    })
        .then(okOrThrow)
        .then(() => dispatch(favourite(movieId)))
        .then(() => {});
}

export function useRemoveFromFavourites(): (id: string) => Promise<void> {
    const authFetch = useAuthenticatedFetch();
    const dispatch = useAppDispatch();
    const userDetails = useAppSelector(s => s.auth.details);

    return movieId => authFetch(`${config.API_URL}/users/${userDetails?.id}/favourites/${movieId}`, {
        method: 'DELETE',
    })
        .then(okOrThrow)
        .then(() => dispatch(unFavourite(movieId)))
        .then(() => {});
}

export function useUpdateFavourites(): (auth: AuthState) => Promise<Array<string>> {
    const authFetch = useAuthenticatedFetch();
    const dispatch = useAppDispatch();

    return (auth) => {
        return authFetch(`${config.API_URL}/users/${auth.details?.id}/favourites`, undefined, auth)
            .then<Array<MovieSummary>>(bodyOrThrow())
            .then(favs => favs.map(movie => movie.id))
            .then(ids => dispatch(setFavourites(ids)))
            .then(d => d.payload);
    }
}