import { useCallback, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { config } from "../config";
import { useAppDispatch, useAppSelector } from "../hooks";
import { AuthState, login, updateDetails } from "../slices/authSlice";
import { bodyOrThrow } from "../utils";
import { useUpdateFavourites } from "./favouriteHooks";

export type UserDetails = {
    id: number,
    email: string,
    username: string,
};

export type PasswordLoginDto = {
    username: string,
    password: string,
};

export type UpdateUserDto = {
    email?: string,
    username?: string,
    password?: string,
};

export function useRegisterUser(): (dto: UpdateUserDto) => Promise<UserDetails> {
    return dto => {
        return fetch(`${config.API_URL}/users`, {
            method: 'POST',
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(dto),
        }).then(bodyOrThrow());
    };
}

export function useLoginUser(): (dto: PasswordLoginDto) => Promise<AuthState> {
    const dispatch = useAppDispatch();
    const updateFavourites = useUpdateFavourites();

    return dto => {
        return fetch(`${config.API_URL}/auth/login`, {
            method: 'POST',
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(dto),
        })
            .then(bodyOrThrow<AuthState>())
            .then(auth => dispatch(login(auth)))
            .then(d => d.payload)
            .then(auth => updateFavourites(auth)
                .then(() => auth));
    };
}

export function useUserDetails()
    : [UserDetails | undefined, (dto: UpdateUserDto) => Promise<UserDetails>] {

    const auth = useAppSelector(s => s.auth);
    const dispatch = useAppDispatch();
    const authFetch = useAuthenticatedFetch();
    const nav = useNavigate();

    useEffect(() => {
        if(!auth.token)
            nav('/');
    }, [auth, nav]);

    const update = (dto: UpdateUserDto) => {
        return authFetch(`${config.API_URL}/users/${auth.details?.id}`, {
            method: 'PATCH',
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(dto),
        })
            .then(bodyOrThrow<UserDetails>())
            .then(d => dispatch(updateDetails(d)))
            .then(d => d.payload);
    }

    return [auth.details ?? undefined, update];
}

export function useAuthenticatedFetch():
    (input: RequestInfo | URL, init?: RequestInit, auth?: AuthState) => Promise<Response> {

    const token = useAppSelector(s => s.auth.token);
    const nav = useNavigate();
    const location = useLocation();

    return useCallback((url, config, auth) => {
        const t = token ?? auth?.token;

        let withAuth = { ...config, };
        if(t) {
            withAuth.headers = {
                ...withAuth.headers,
                'Authorization': `Bearer ${t}`,
            };
        }

        let redirectUri = location.pathname
            ? `?redirectUri=${encodeURIComponent(location.pathname)}`
            : '';

        return fetch(url, withAuth)
            .then(resp => {
                if(resp.status === 401) {
                    nav(`/login${redirectUri}`);
                    return Promise.reject('Invalid token');
                }

                return resp;
            });
    }, [token, nav, location.pathname]);
}