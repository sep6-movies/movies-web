import { useCallback, useEffect, useState } from "react";
import { config } from "../config";
import { useAppSelector } from "../hooks";
import { bodyOrThrow } from "../utils";
import { useAuthenticatedFetch } from "./userHooks";

export type UpdateReviewDto = {
    title: string,
    content: string,
};

export type ReviewDto = {
    reviewerId: number,
    reviewer: string,
    movieId: string,
    title: string,
    content: string,
};

export function useUserReview(movieId?: string): [ReviewDto | undefined, (dto: UpdateReviewDto) => Promise<void>, boolean] {
    const authFetch = useAuthenticatedFetch();
    const user = useAppSelector(s => s.auth.details);
    const [review, setReview] = useState<ReviewDto | undefined>();
    const [updating, setUpdating] = useState(false);

    useEffect(() => {
        if(!user || !movieId)
            return;

        setUpdating(true);
        authFetch(`${config.API_URL}/movies/${movieId}/reviews/${user.id}`)
            .then(resp => resp.ok ? resp.json() : { reviewerId: user.id, reviewer: user.username, movieId })
            .then(r => setReview(r))
            .finally(() => setUpdating(false));
    }, [movieId, user, authFetch]);

    const updateReview = useCallback((dto: UpdateReviewDto) => {
        if(!movieId || !user)
            return Promise.reject();

        setUpdating(true);
        return authFetch(`${config.API_URL}/movies/${movieId}/reviews/${user.id}`, {
            method: 'PUT',
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(dto),
        })
            .then(bodyOrThrow<ReviewDto>())
            .then(r => setReview(r))
            .finally(() => setUpdating(false));
    }, [movieId, user, authFetch]);

    return [review, updateReview, updating];
}

export function useMovieReviews(movieId?: string): [Array<ReviewDto> | undefined, boolean] {
    const [reviews, setReviews] = useState<Array<ReviewDto> | undefined>();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(!movieId)
            return;

        setLoading(true);
        fetch(`${config.API_URL}/movies/${movieId}/reviews`)
            .then(bodyOrThrow<Array<ReviewDto>>())
            .then(rs => setReviews(rs))
            .finally(() => setLoading(false));
    }, [movieId]);

    return [reviews, loading];
}
