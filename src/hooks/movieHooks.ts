import { useCallback, useEffect, useState } from "react";
import { config } from "../config";
import { bodyOrThrow } from "../utils";
import { RatingSummary } from "./ratingHooks";

export type CastMember = {
    id: string,
    name: string,
    character: string,
    profile_path?: string,
};

export type CrewMember = {
    id: string,
    name: string,
    job: string,
    profile_path?: string,
};

export type MovieDetails = {
    id: string,
    title: string,
    posterUrl: string,
    description: string,
    cast: Array<CastMember>,
    crew: Array<CrewMember>,
    rating: RatingSummary,
};

export type MovieSummary = {
    id: string,
    title: string,
    posterUrl: string,
    description: string,
    avgRating: number,
};

export function useMovieDetails(movieId?: string): [MovieDetails | undefined, (updated: MovieDetails) => void, boolean, string | null] {
    const [details, setDetails] = useState<MovieDetails | undefined>();
    const [loading, setLoading] = useState(false);
    const [err, setErr] = useState<string | null>(null);

    useEffect(() => {
        if(!movieId)
            return;

        setLoading(true);
        fetch(`${config.API_URL}/movies/${movieId}`)
            .then(bodyOrThrow<MovieDetails>())
            .then(d => setDetails(d))
            .catch(e => setErr(e.message))
            .finally(() => setLoading(false));
    }, [movieId]);

    return [details, setDetails, loading, err];
}

export function usePopularMovies(): [Array<MovieSummary> | undefined, string | null, boolean] {
    const [popular, setPopular] = useState<Array<MovieSummary> | undefined>();
    const [loading, setLoading] = useState(false);
    const [err, setErr] = useState<string | null>(null);

    useEffect(() => {
        setLoading(true);
        fetch(`${config.API_URL}/movies/popular`)
            .then(bodyOrThrow<Array<MovieSummary>>())
            .then(p => setPopular(p))
            .catch(e => setErr(e.message))
            .finally(() => setLoading(false));
    }, []);

    return [popular, err, loading];
}

export function useMovieSearch(): [Array<MovieSummary> | undefined, (p: string) => void, string | null, boolean] {
    const [results, setResults] = useState<Array<MovieSummary> | undefined>();
    const [loading, setLoading] = useState(false);
    const [err, setErr] = useState<string | null>(null);

    const search = useCallback((phrase: string) => {
        setLoading(true);
        setResults(undefined);
        fetch(`${config.API_URL}/movies/search?query=${phrase}`)
            .then(bodyOrThrow<Array<MovieSummary>>())
            .then(res => setResults(res))
            .catch(e => setErr(e.message))
            .finally(() => setLoading(false));
    }, []);

    return [results, search, err, loading];
}
