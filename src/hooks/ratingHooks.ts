import { useCallback, useEffect, useState } from "react";
import { config } from "../config";
import { bodyOrThrow } from "../utils";
import { useAuthenticatedFetch } from "./userHooks";

export type RatingSummary = {
    avg: number,
    counts: {
        [key: string]: number;
    },
};

export type RatingDto = {
    value: number,
};

export function useUserRating(movieId: string | null, reviewerId: number | null): [number, (dto: RatingDto) => Promise<RatingSummary>, () => Promise<RatingSummary>, boolean] {
    const authFetch = useAuthenticatedFetch();
    const [rating, setRating] = useState<number>(0);
    const [updating, setUpdating] = useState(false);

    useEffect(() => {
        if(!movieId || !reviewerId)
            return;

        setUpdating(true);
        fetch(`${config.API_URL}/movies/${movieId}/ratings/${reviewerId}`)
            .then(bodyOrThrow<RatingDto>())
            .then(r => setRating(r.value))
            .catch(() => setRating(0))
            .finally(() => setUpdating(false));
    }, [movieId, reviewerId]);

    const rate = useCallback((dto: RatingDto) => {
        if(!movieId || !reviewerId)
            return Promise.reject();

        let url = `${config.API_URL}/movies/${movieId}/ratings`;
        if(rating !== 0)
            url = `${url}/${reviewerId}`;

        setUpdating(true);
        return authFetch(url, {
            method: rating === 0 ? 'POST' : 'PUT',
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify({ ...dto, userId: reviewerId }),
        })
            .then(bodyOrThrow<RatingSummary>())
            .then(summary => {
                setRating(dto.value);
                return summary;
            })
            .finally(() => setUpdating(false));
    }, [movieId, reviewerId, rating, authFetch]);

    const deleteRating = useCallback(() => {
        if(!movieId || !reviewerId)
            return Promise.reject();

        setUpdating(true);
        return authFetch(`${config.API_URL}/movies/${movieId}/ratings/${reviewerId}`, {
            method: 'DELETE',
        })
            .then(bodyOrThrow<RatingSummary>())
            .then(dto => {
                setRating(0);
                return dto;
            })
            .finally(() => setUpdating(false));
    }, [movieId, reviewerId, authFetch]);

    return [rating, rate, deleteRating, updating];
}
