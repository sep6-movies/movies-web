import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { MovieSummary } from '../../hooks/movieHooks';
import MovieTile from '../MovieTile/MovieTile';
import "./MovieCarousel.css";

export type MovieCarouselProps = {
    movies: Array<MovieSummary>
};

function MovieCarousel(props: MovieCarouselProps) {
    const responsive = {
        desktop: {
            breakpoint: {max: 3000, min: 1024},
            items: 2,
            slidesToSlide: 2
        },
        tablet: {
            breakpoint: {max: 1024, min: 464},
            items: 1,
            slidesToSlide: 1
        },
        mobile: {
            breakpoint: {max: 464, min: 0},
            items: 1,
            slidesToSlide: 1
        }
    };

    const movieTiles = props.movies
        .map((m, idx) => <MovieTile key={idx} movie={m} />);

    return (
        <Carousel
            arrows
            autoPlay={true}
            autoPlaySpeed={1000 * 10}
            containerClass="carousel-container pb-4"
            dotListClass="d-none d-md-flex"
            itemClass="pe-2"
            infinite={true}
            pauseOnHover
            renderButtonGroupOutside={true}
            responsive={responsive}
            showDots={true}>

            {movieTiles}
        </Carousel>
    );
}

export default MovieCarousel;