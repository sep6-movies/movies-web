import { MouseEvent, useEffect, useState } from "react";
import { Heart, HeartFill } from "react-bootstrap-icons";
import { useAppSelector } from "../../hooks";
import { useAddToFavourites, useRemoveFromFavourites } from "../../hooks/favouriteHooks";

export default function FavouriteButton(props: { id?: string, size?: string | number, buttonStyle?: string }) {
    const [isFavourite, setIsFavourite] = useState(false);
    const fav = useAppSelector(s => s.fav.favourites);
    const addToFav = useAddToFavourites();
    const removeFromFav = useRemoveFromFavourites();

    useEffect(() => {
        if(!props.id) return;
        setIsFavourite(fav.includes(props.id));
    }, [fav, props.id]);

    const toggleFav = (e: MouseEvent<SVGElement>) => {
        e.stopPropagation();

        if(!props.id) return;
        isFavourite ? removeFromFav(props.id) : addToFav(props.id);
    };

    const buttonProps = {
        height: '100%',
        width: '2.5em',
        color: 'firebrick',
        className: `align-top hover-lift ${props.buttonStyle ?? ''}`,
        onClick: (e: MouseEvent<SVGElement>) => toggleFav(e),
    };

    return (
        isFavourite
            ? <HeartFill {...buttonProps} />
            : <Heart {...buttonProps} />
    );
}
