import { Spinner } from "react-bootstrap";
import { Star, StarFill } from "react-bootstrap-icons";
import './RatingView.css';

export type RatingViewProps = {
    value: number | null,
    readOnly?: boolean,
    onChange?: (value: number) => void,
    className?: string,
    loading?: boolean,
    labelled?: boolean,
};

function RatingView(props: RatingViewProps) {
    const handleChange = (value: number) => {
        if(props.readOnly || props.loading)
            return;
        props.onChange?.(value + 1);
    };

    const opacity = props.loading ? 0.75 : 1;
    const color = `rgba(218, 165, 32, ${opacity})`;

    const stars = Array.from({ length: 5 })
        .map((_, i) => i < (props.value ?? 0) - 0.5
            ? <StarFill key={i} color={color} className="rating-star"
                onClick={() => handleChange(i)} />
            : <Star key={i} color={color} className="rating-star"
                onClick={() => handleChange(i)} />);

    const classes = [];
    if(!props.readOnly)
        classes.push('editable');

    if(props.className)
        classes.push(props.className);

    return (
        <div className={`rating-container gap-2 fs-2 ${classes.join(' ')}`}>
            {stars}

            {props.labelled && !!props.value && props.value > 0
                && <span>{props.value.toFixed(1)}</span>}

            {props.loading && <Spinner animation="border" className="inline-spinner" />}
        </div>
    );
}

export default RatingView;
