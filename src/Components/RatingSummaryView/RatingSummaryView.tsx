import { Card, ProgressBar, Stack } from "react-bootstrap";
import { StarFill } from "react-bootstrap-icons";
import { RatingSummary } from "../../hooks/ratingHooks";

export type RatingSummaryViewProps = {
    summary?: RatingSummary,
};

function RatingSummaryView(props: RatingSummaryViewProps) {
    if(!props.summary)
        return <></>;

    const ratingValues = Array.from({ length: 5 }).map((_, idx) => `${5 - idx}`);
    const counts = props.summary.counts;

    const total = Object.values(counts).reduce((acc, next) => acc + next, 0);
    const ratingLine = (ratingValue: string, progress: number) => (
        <Stack key={ratingValue} direction="horizontal">
            <span style={{ width: '0.75em' }}>{ratingValue}</span>
            <StarFill color="goldenrod" className="mx-2" />
            <ProgressBar now={progress} className="flex-grow-1" />
        </Stack>
    );
    const lines = ratingValues.map(k => ratingLine(k, counts[k] ? (counts[k] / total) * 100 : 0));

    return (
        <Card>
            <Card.Body className="d-flex flex-column gap-2">
                <Card.Title className="d-flex align-items-center fs-1">
                    {props.summary.avg === 0 ? (
                        <>No rating yet</>
                    ) : (
                        <>{ props.summary.avg.toFixed(1) } <StarFill color="goldenrod" className="mx-2" /> stars</>
                    )}
                </Card.Title>

                <Card.Subtitle>
                    {total} vote{total !== 1 && 's'}
                </Card.Subtitle>
                <Stack gap={2}>
                    {lines}
                </Stack>
            </Card.Body>
        </Card>
    );
}

export default RatingSummaryView;