import { FormEvent, useState } from "react";
import { Alert, Button, Card, FloatingLabel, Form, Spinner } from "react-bootstrap";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useLoginUser } from "../../hooks/userHooks";

export default function LoginForm() {

    const [params] = useSearchParams();
    const login = useLoginUser();
    const nav = useNavigate();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<string | null>();

    const handleLogin = (e: FormEvent) => {
        e.preventDefault();

        let redirect = params.get('redirectUri');
        redirect = redirect ? decodeURIComponent(redirect) : '/';

        setLoading(true);
        setError(null);
        login({ username, password })
            .then(() => nav(redirect!))
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    };

    return (
        <Card className="shadow" body>
            <Card.Title className="mb-3">Login</Card.Title>

            {error && <Alert variant="danger" dismissible>
                {error}
            </Alert>}

            <Form onSubmit={handleLogin} className="d-flex flex-column gap-3">

                <FloatingLabel controlId="usernameControl" label="Username">
                    <Form.Control
                        required
                        type="text"
                        size="sm"
                        placeholder="Username"
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        disabled={loading} />
                </FloatingLabel>

                <FloatingLabel controlId="passwordControl" label="Password">
                    <Form.Control
                        required
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        disabled={loading} />
                </FloatingLabel>

                <Button type="submit" disabled={loading}>
                    {loading && <Spinner size="sm" animation="border" className="me-2" />}
                    Login
                </Button>
            </Form>
        </Card>
    );
}
