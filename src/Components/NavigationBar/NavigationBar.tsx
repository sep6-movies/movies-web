import { FormEvent, useState } from "react";
import { Nav, Navbar, Offcanvas } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import { Link, useNavigate } from "react-router-dom";
import ProfileNavButton from "../ProfileNavButton/ProfileNavButton";

function NavigationBar() {

    const nav = useNavigate();
    const [searchPhrase, setSearchPhrase] = useState('');

    const [menuOpened, setMenuOpened] = useState(false);
    const handleMenuToggle = () => setMenuOpened(e => !e);

    const handleSearch = (e: FormEvent) => {
        e.preventDefault();

        setMenuOpened(false);
        const query = encodeURIComponent(searchPhrase);
        nav(`/search?query=${query}`);
    };

    return (
        <Navbar
            variant="dark"
            bg="dark"
            fixed="top"
            collapseOnSelect
            expanded={menuOpened}
            onToggle={handleMenuToggle}
            expand="md"
            className="shadow">

            <Container fluid>
                <Navbar.Brand as={Link} to="/">IMDB Clone</Navbar.Brand>

                <Navbar.Toggle aria-controls="navbar-offcanvas"/>
                <Navbar.Offcanvas id="navbar-offcanvas" placement="end">
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title>
                            <Nav.Link as={Link} to="/" eventKey={0}>IMDB Clone</Nav.Link>
                        </Offcanvas.Title>
                    </Offcanvas.Header>

                    <Offcanvas.Body>
                        <Form className="d-flex flex-grow-1 px-md-5" onSubmit={handleSearch}>
                            <Form.Control
                                type="search"
                                placeholder="Search"
                                className="me-2"
                                aria-label="Search"
                                value={searchPhrase}
                                onChange={e => setSearchPhrase(e.target.value)} />

                            <Button variant="outline-success" type="submit">Search</Button>
                        </Form>

                        <Nav className="justify-content-end my-2 my-md-0">
                            <Nav.Link as={Link} to="/" eventKey={0}>Home</Nav.Link>
                            <ProfileNavButton />
                        </Nav>
                    </Offcanvas.Body>
                </Navbar.Offcanvas>
            </Container>
        </Navbar>
    );
}

export default NavigationBar;
