import { CSSProperties } from "react";
import { Spinner } from "react-bootstrap";

function CenteredSpinner(props: { styleClass?: CSSProperties }) {

    return (
        <div className="d-flex justify-content-center p-3">
            <Spinner animation="border" style={props.styleClass} />
        </div>
    );
}

export default CenteredSpinner;
