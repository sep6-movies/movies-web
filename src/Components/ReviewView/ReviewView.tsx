import { FormEvent, useEffect, useState } from "react";
import { Badge, Button, Card, Form, Modal } from "react-bootstrap";
import { RatingSummary, useUserRating } from "../../hooks/ratingHooks";
import { ReviewDto, UpdateReviewDto } from "../../hooks/reviewHooks";
import RatingView from "../RatingView/RatingView";

export type ReviewViewProps = {
    review?: ReviewDto,
    readOnly?: boolean,
    loading?: boolean,
    onRatingChange?: (value: RatingSummary) => void,
    onReviewChange?: (reviewDto: UpdateReviewDto) => void,
    className?: string,
};

function ReviewView(props: ReviewViewProps) {
    const [rating, rate, deleteRating, updatingRating] = useUserRating(
        props.review?.movieId ?? null, props.review?.reviewerId ?? null);

    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [showConfirmDelete, setShowConfirmDelete] = useState(false);

    useEffect(() => {
        setTitle(props.review?.title ?? '');
        setContent(props.review?.content ?? '');
    }, [props.review]);

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(!props.readOnly)
            props.onReviewChange?.({ title, content });
    };

    const handleRatingChange = (value: number) => {
        if(value === rating && props.review) {
            setShowConfirmDelete(true);
            return;
        }

        const update = value === rating ? deleteRating() : rate({ value });
        update.then(dto => props.onRatingChange?.(dto));
    };

    const handleDeleteRating = () => {
        deleteRating()
            .then(dto => props.onRatingChange?.(dto))
            .finally(() => setShowConfirmDelete(false));
    };

    return (
        <Card body className={props.className}>
            <Form onSubmit={handleSubmit} className="d-flex flex-column gap-2">

                <h3 className="mb-0">
                    <Badge pill className="align-self-start">
                        {props.review?.reviewer}
                    </Badge>
                </h3>

                <RatingView
                    value={rating ?? 0}
                    onChange={handleRatingChange}
                    loading={updatingRating || !!props.loading}
                    readOnly={props.readOnly} />

                {!!rating && <>
                    <Form.Control
                        type="text"
                        className="form-control-lg"
                        placeholder="title"
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                        plaintext={props.readOnly}
                        readOnly={props.readOnly}
                        disabled={!!props.loading} />

                    <Form.Control
                        as="textarea"
                        rows={5}
                        value={content}
                        onChange={e => setContent(e.target.value)}
                        plaintext={props.readOnly}
                        readOnly={props.readOnly}
                        disabled={!!props.loading} />

                    {props.readOnly || <Button type="submit" disabled={!!props.loading}>
                        Post review
                    </Button>}
                </>}

                <Modal show={showConfirmDelete} onHide={() => setShowConfirmDelete(false)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure?</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        Deleting your rating will also remove the review. Are you sure?
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => setShowConfirmDelete(false)}>
                            Cancel
                        </Button>
                        <Button variant="danger" onClick={handleDeleteRating}>
                            Delete
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Form>
        </Card>
    );
}

export default ReviewView;
