import { Card } from "react-bootstrap";
import { Film } from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";
import { MovieSummary } from "../../hooks/movieHooks";
import FavouriteButton from "../FavouritesButton/FavouritesButton";
import RatingView from "../RatingView/RatingView";
import './MovieTile.css';

export type MovieTileProps = {
    movie: MovieSummary | null,
    styleClass?: string,
};

function MovieTile(props: MovieTileProps) {
    const nav = useNavigate();

    const navTo = (id?: string) => {
        if(id)
            nav(`/movies/${id}`);
    };

    let summary = props.movie?.description;

    return (
        <Card
            className={`movie-tile flex-column flex-md-row ${props.styleClass ?? ''}`}
            onClick={() => navTo(props.movie?.id)}>

            {props.movie?.posterUrl 
                ? (
                    <Card.Img
                        src={props.movie.posterUrl}
                        alt={props.movie?.title}
                        className="flex-1 p-3 rounded" />
                ) : (
                    <div className="poster-placeholder flex-1 p-4">
                        <Film />
                    </div>
                )}

            <Card.Body className="flex-1 d-flex flex-column">
                <Card.Title>
                    {props.movie?.title}
                </Card.Title>

                <RatingView value={props.movie?.avgRating ?? null} readOnly labelled />

                <Card.Text className="flex-grow-1">{summary}</Card.Text>

                <FavouriteButton id={props.movie?.id} buttonStyle="align-self-end flex-0-1-content" />
            </Card.Body>
        </Card>
    );
}

export default MovieTile;