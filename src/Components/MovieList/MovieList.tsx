import { Col, Container, Row } from "react-bootstrap";
import { MovieSummary } from "../../hooks/movieHooks";
import MovieTile from "../MovieTile/MovieTile";

function MovieList(props: { movies: Array<MovieSummary> }) {

    return (
        <Container fluid>
            <Row>
                {props.movies?.length === 0 && <Col>No movies found</Col>}

                {props.movies?.map((m, idx) => <Col key={idx} xs={12} lg={6} className="mb-3">
                    <MovieTile movie={m} />
                </Col>)}
            </Row>
        </Container>
    );
}

export default MovieList;