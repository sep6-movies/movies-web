import Container from "react-bootstrap/Container";
import './Footer.css';

function NavigationBar() {

    return (
        <Container fluid className="d-flex justify-content-end p-2 bg-dark text-white">
            <a href="https://www.themoviedb.org" className="technology-container" target="_blank" rel="noreferrer">
                Powered by The Movie Database
                <img
                    alt="The Movie Database"
                    src="https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg"
                    className="technology-logo" />
            </a>
        </Container>
    );
}

export default NavigationBar;
