import { FormEvent, useEffect, useState } from "react";
import { Alert, Button, Card, FloatingLabel, Form, Spinner } from "react-bootstrap";
import { UpdateUserDto, UserDetails } from "../../hooks/userHooks";

export type UserDetailsFormProps = {
    title: string,
    onSubmit: (dto: UpdateUserDto) => Promise<any>,
    details?: UserDetails,
    buttonLabel?: string,
};

function UserDetailsForm(props: UserDetailsFormProps) {

    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<string | null>();

    useEffect(() => {
        setEmail('');
        setUsername('');
        setPassword('');
        setConfirmPassword('');
    }, [props.details]);

    const undefinedIfEmpty = (s: string) => s.length === 0 ? undefined : s;

    const handleError = (err: any) => {
        if(err.errors) {
            setError(err.errors
                .map((e: any) => e.defaultMessage)
                .reduce((acc: string, next: string) => acc + next, ''));
        } else {
            setError(err.message);
        }
    };

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();

        if(password !== confirmPassword) {
            setError("Passwords do not match");
            return;
        }

        setLoading(true);
        setError(null);
        props.onSubmit({
            username: undefinedIfEmpty(username),
            email: undefinedIfEmpty(email),
            password: undefinedIfEmpty(password),
        })
            .catch(err => handleError(err))
            .finally(() => setLoading(false));
    }

    const buttonDisabled = loading
        || [username, email, password].every(e => !e);

    return (
        <Card className="shadow" body>
            <Card.Title className="mb-3">{props.title}</Card.Title>

            {error && <Alert variant="danger" dismissible onClose={() => setError(null)}>
                {error}
            </Alert>}

            <Form onSubmit={handleSubmit} className="d-flex flex-column gap-3">

                <FloatingLabel controlId="emailControl" label={props.details?.email ?? "Email"}>
                    <Form.Control
                        type="email"
                        size="sm"
                        placeholder="Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        disabled={loading} />
                </FloatingLabel>

                <FloatingLabel controlId="usernameControl" label={props.details?.username ?? "Username"}>
                    <Form.Control
                        type="text"
                        placeholder="Username"
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        disabled={loading} />
                </FloatingLabel>

                <FloatingLabel controlId="passwordControl" label={props.details ? "New password" : "Password"}>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        disabled={loading} />
                </FloatingLabel>

                <FloatingLabel controlId="confirmPasswordControl" label="Confirm password">
                    <Form.Control
                        type="password"
                        placeholder="Confirm password"
                        value={confirmPassword}
                        onChange={e => setConfirmPassword(e.target.value)}
                        disabled={loading} />
                </FloatingLabel>

                <Button type="submit" disabled={buttonDisabled}>
                    {loading && <Spinner size="sm" className="me-2" animation="border" />}
                    {props.buttonLabel ?? props.title}
                </Button>
            </Form>
        </Card>
    );
}

export default UserDetailsForm;