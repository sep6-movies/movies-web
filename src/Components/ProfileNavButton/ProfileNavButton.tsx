import { Nav } from "react-bootstrap";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { logout } from "../../slices/authSlice";

function ProfileNavButton() {

    const user = useAppSelector(s => s.auth.details);
    const dispatch = useAppDispatch();
    const nav = useNavigate();
    const location = useLocation();

    const handleLogin = () => {
        const redirectQuery = `?redirectUri=${encodeURIComponent(location.pathname)}`;
        nav(`/login${redirectQuery}`);
    };

    const handleLogout = () => {
        dispatch(logout());
        nav('/');
    };

    if(user) {
        return (
            <>
                <Nav.Link as={Link} to="/my-profile" eventKey={0}>Profile</Nav.Link>
                <Nav.Link as={Link} to="/favourites" eventKey={0}>Favourites</Nav.Link>
                <Nav.Link onClick={handleLogout}>Log out</Nav.Link>
            </>
        );
    } else {
        return (
            <>
                <Nav.Link onClick={handleLogin} eventKey={0}>Login</Nav.Link>
                <Nav.Link as={Link} to="/register" eventKey={0}>Register</Nav.Link>
            </>
        );
    }
}

export default ProfileNavButton;
