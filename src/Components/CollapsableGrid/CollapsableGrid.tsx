import { MouseEvent, useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { PersonFill } from "react-bootstrap-icons";
import CenteredSpinner from "../CenteredSpinner/CenteredSpinner";

export type GridItemData = {
    title: string,
    description: string,
    image?: string,
};

const GridItem = (data: GridItemData) => (
    <Card className="h-100">
        {data.image
            ? <Card.Img variant="top" src={data.image} />
            : <PersonFill size="100%" className="flex-grow-1" />}

        <Card.Body className="flex-grow-0">
            <Card.Title>{data.title}</Card.Title>
            <Card.Subtitle>{data.description}</Card.Subtitle>
        </Card.Body>
    </Card>
);

function CollapsableGrid(props: {
    header: string,
    items?: Array<GridItemData>,
    loading: boolean,
}) {
    const [displayedItems, setDisplayedItems] = useState<GridItemData[] | undefined>();
    const [expanded, setExpanded] = useState(false);

    const buttonHandler = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        setExpanded(e => !e);
    };

    useEffect(() => {
        setDisplayedItems(() => expanded ? props.items : props.items?.slice(0, 4));
    }, [props.items, expanded]);

    return (
        <>
            <h2 className="mb-3">{props.header}</h2>

            {props.loading ? (
                <CenteredSpinner styleClass={{ margin: "100px 0" }} />
            ) : (
                <Container fluid>
                    <Row>
                        {displayedItems?.map((item, idx) =>
                            <Col xs={6} md={3} key={idx} className="mb-3">
                                <GridItem {...item} />
                            </Col>)}
                    </Row>

                    {props.items && props.items.length > 4 && <Row>
                        <Button className="w-100" onClick={buttonHandler}>{expanded ? 'Show less...' : 'Show more...'}</Button>
                    </Row>}
                </Container>
            )}
        </>
    );
}

export default CollapsableGrid;
