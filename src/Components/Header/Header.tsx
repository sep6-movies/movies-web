import { ReactElement } from "react";
import { Col, Row } from "react-bootstrap";

export type HeaderProps = {
    header: ReactElement | string,
    children?: ReactElement | ReactElement[] | null,
};

function Header(props: HeaderProps) {

    let header = props.header;
    if(typeof header === 'string')
        header = <h1 className="mb-4">{header}</h1>;

    return (
        <Row>
            <Col>
                {header}
            </Col>

            {props.children && <Col className="flex-grow-0">
                {props.children}
            </Col>}
        </Row>
    );
}

export default Header;