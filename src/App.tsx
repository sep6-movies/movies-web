import { useEffect } from "react";
import { Outlet, useLocation } from "react-router-dom";
import './App.css';
import Footer from "./Components/Footer/Footer";
import NavigationBar from "./Components/NavigationBar/NavigationBar";

function App() {

    const location = useLocation();
    useEffect(() => {
        window.scrollTo(0, 0);
    }, [location]);

    return (
        <>
            <NavigationBar />
            <Outlet />
            <Footer />
        </>
    );
}

export default App;
