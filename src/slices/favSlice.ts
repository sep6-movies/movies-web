import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface FavState {
    favourites: Array<string>;
}

const initialState: FavState = {
    favourites: [],
};

export const favSlice = createSlice({
    name: 'fav',
    initialState,
    reducers: {
        setFavourites: (state, action: PayloadAction<Array<string>>) => {
            state.favourites = action.payload;
        },
        favourite: (state, action: PayloadAction<string>) => {
            state.favourites = state.favourites.concat(action.payload);
        },
        unFavourite: (state, action: PayloadAction<string>) => {
            state.favourites = state.favourites.filter(id => id !== action.payload);
        },
    },
});

export const {setFavourites, favourite, unFavourite} = favSlice.actions;

export default favSlice.reducer;