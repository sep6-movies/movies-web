import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { UserDetails } from '../hooks/userHooks';

export interface AuthState {
    details: UserDetails | null;
    token: string | null;
}

const initialState: AuthState = {
    details: null,
    token: null,
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: (state, action: PayloadAction<AuthState>) => {
            state.details = action.payload.details;
            state.token = action.payload.token;
        },
        logout: state => {
            state.details = null;
            state.token = null;
        },
        updateDetails: (state, action: PayloadAction<UserDetails>) => {
            state.details = action.payload;
        },
    },
});

export const { login, logout, updateDetails } = authSlice.actions;

export default authSlice.reducer;